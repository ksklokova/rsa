#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "LNcppDll.h"

#pragma comment (lib, "LNcppDll.lib")

				//MAIN
int main(int argc, char *argv[])
{
	if (argc<5&&(argc>7))
	{
		perror("Wrong arguments!");
		return -1;
	}

	char operation;
	if (strlen(argv[2])>1)
	{
		perror("Wrong operator!");
		return -2;
	}
	else operation=argv[2][0];

	LongNum num1, num2, num0, mod;
	bool is_bin, by_mod;

	//���� ����� ��������
	if (((argc==6)&&(argv[5][0]=='-')&&(argv[5][1]=='b'))||((argc==7)&&(argv[6][0]=='-')&&(argv[6][1]=='b')))
	{
		is_bin=true;
		if (argc==7) by_mod=true;
		else by_mod=false;
	}
	else
	{
		is_bin=false;		
		if (argc==6) by_mod=true;
		else by_mod=false;
	}


	num1.input(argv[1], is_bin);
	num2.input(argv[3], is_bin);
	//if (is_bin) num0.input(argv[4], is_bin);

	if (by_mod)
		mod.input(argv[5], is_bin);

	if (operation=='+')
		num0=(num1+num2);	
	else if (operation=='-')
		num0=num1-num2;
	else if (operation=='*')
		num0=num1*num2;
	else if (operation=='/')
		num0=num1/num2;
	else if (operation=='%')
		num0=num1%num2;
	else if (operation=='^'&&by_mod)
		num0=num1.deg_by_mod(num2, mod);
	else 
	{
		perror("Wrong operator!\n");
		return 2;
	}

	if (is_bin)
	{
		num0.rename_b(argv[4]);
		//remove("tmpfile");
	}
	

	if (by_mod&&operation!='^')
	{
		LongNum tmp;
		tmp=num0%mod;
		tmp.output(argv[4]);
		//num0.rename_b(argv[4]);
		//remove("tmpfile");
	}
	else
	if (!is_bin) num0.output(argv[4]);

	return 0;
}
