//LNDll.c 
//���������� �������� ��� ������ � �������� �������

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "LNDll.h"

int max(int a, int b)
{
	if (a>=b)
		return a;
	return b;
}
int min(int a, int b)
{
	if (a<=b)
		return a;
	return b;
}

//                       ******************************************
//                       *                                        *
//                       *   ������� ������ � ����������� ������� *
//                       *                                        *
//                       ******************************************

//======================================== ���� �� ���������� ����� ===============================================
BigInt BI_input(FILE *f)
{														//�� ���� ������� �������� ��������� �� ��� �������� ���������� ���� � ������
														//����� � ������ ����� ��������� ��������, �.�. ������� ��������� "�����", ����� ������
														//"�����" ������������ ����� �������������� ������������� ����� (int<10000)
	int i, j, k, str_size;
	char *snum, ch, block[osn_size+1];
	block[osn_size]=0;
	BigInt bnum;
														//������� ������ (���� ��� �� ��������)
	snum=(char*)malloc(max_size*sizeof(char));
	str_size=1;
	fscanf(f, "%c", &ch);
														//���������, ������������� ��
	if (ch=='-')
	{
		bnum.minus=true;
		fscanf(f, "%c", &ch);
	}
	else bnum.minus=false;

	while (ch=='0')										//��������� ���� � ������
		fscanf(f, "%c", &ch);
	while(1)
	{
		if ((ch<'0')||(ch>'9')||feof(f))
		{
			//snum[str_size-1]=0;
			break;
		}
		snum[str_size-1]=ch;
		str_size++;
		if (str_size>max_size)
			snum=(char *)realloc(snum, (str_size)*sizeof(char));
		fscanf(f, "%c", &ch);
	}
	str_size--;
//printf("str_size=%d\n", str_size);
					
														//���������� ���������� "����" � ������� ����� 
	if (str_size%osn_size==0)
		bnum.amount = str_size/osn_size;
	else
		bnum.amount = str_size/osn_size +1;

//printf("bnum.amount=%d\n", bnum.amount);

	bnum.digit = (int *)malloc(bnum.amount*sizeof(int));
	
														//�������� � ������ ����� "�����" � �������� �������
	for (i=0; i<bnum.amount; i++)
	{
		if (i==bnum.amount-1)
			for (j=0; j<osn_size; j++)
				block[j]='0';
														//������ ������ "�����" ������� � ���������� �������
		for (k=0; k<osn_size; k++)
		{
			j=str_size-(1+ i*osn_size+ k);
			ch=snum[j];
			if ((ch>='0')&&(ch<='9'))
				block[osn_size-(k+1)]=snum[j];		
			else
				break;
		}
		
//printf("%s = ", block);
		
		bnum.digit[i] = atoi(block);
//printf("%d\n", bnum.digit[i]);
	}
	free(snum);
	return bnum;
}

//======================================== ����� � ��������� ���� ===============================================
void BI_output(BigInt bnum, FILE *f)
{														//�� ���� �������� ������� ����� � ��������� �� ��� �������� ����, ���� ���������� ������
	int i, j, d;
	char block[osn_size+1];
	block[osn_size]=0;
	if (!(bnum.amount==1&&bnum.digit[0]==0)&&bnum.minus==true)	fprintf(f, "-");

	fprintf(f, "%d", bnum.digit[bnum.amount-1]);
	for (i=bnum.amount-2; i>=0; i--)					//������� � ���� ��� "�����" � �������� �������
	{
    	d=bnum.digit[i];
		for (j=osn_size-1; j>=0; j--)
		{
			block[j]= '0'+ d%10;
			d/=10;
		}
		fprintf(f, "%s", block);
	}
}

//=========================================== ��������� �� ������ ===================================================
int BI_comp(BigInt num1, BigInt num2)
{														//������� ��������� ��� ���������� "�����" � �����
	if (num1.amount>num2.amount)
		return 1;
	if (num1.amount<num2.amount)
		return -1;
	int i;
	for (i=num1.amount-1; i>=0; i--)					//���� ���� ���������� ���������� - ��������� �� ������� �������� � �������
	{													//���� �� �������� �������
		if (num1.digit[i]>num2.digit[i])
			return 1;
		if (num1.digit[i]<num2.digit[i])
			return -1;
	}
	return 0;
}

//======================================== �������� ===============================================
BigInt BI_add(BigInt num1, BigInt num2)
{
BigInt res;
if (num1.minus==num2.minus)								//���� ������������ ����� ����������� ����� 
{
	res.minus=num1.minus;
	int r=0,i, d1, d2;

	if (num1.amount>=num2.amount)						//��������� ���� ������� �����
		res.amount=num1.amount;
	else res.amount=num2.amount;

	res.digit = (int*)malloc(res.amount*sizeof(int));	//���������� �������� (��� ��� �������������)
//printf("\n*\nres.amount=%d\n", res.amount);
	for (i=0; i<res.amount; i++)
	{
		if (i>=num1.amount)	d1=0; else d1=num1.digit[i];
		if (i>=num2.amount)	d2=0; else d2=num2.digit[i];

		res.digit[i]=d1+d2+r;							//� ������ �������� r
		if (res.digit[i]>=osn)
		{
			res.digit[i]-=osn;
			r=1;
		}
		else
			r=0;
//printf("\n%d", res.digit[i]);
	}
	if (r==1)
	{
		res.amount++;
		res.digit=(int*)realloc(res.digit, res.amount*sizeof(int));
		res.digit[res.amount-1]=1;
//printf("\n%d", res.digit[res.amount-1]);
	}
}
else if (num1.minus==true)								//���� ������������ ����� ������ ������ - ��� �� ���� ���������
{
	num1.minus=false;
	res = BI_sub(num2, num1);
}
else
{
	num2.minus=false;
	res=BI_sub(num1, num2);
}
	return res;
}

//=========================================== ��������� =======================================================
BigInt BI_sub(BigInt num1, BigInt num2)
{
	BigInt res;
	if (!num1.minus&&num2.minus) //(+)-(-) = (+)+(+)	//���� ��������� ������ ������, ��� �� ���� ��������
	{																		
		num2.minus=false;
		res=BI_add(num1, num2);
		return res;
	}
	else if (num1.minus&&!num2.minus) //(-)-(+)= -((+)+(+))
	{
		num1.minus=false;
		res=BI_add(num1,num2);
		res.minus=true;
		return res;
	}
	else  //(-5)-(-3)=-(5-3) or (-3)-(-5)=5-3 or 3-5=-(5-3) or 5-3
	{													//���� ���-���� ������������ ���������
		int c=BI_comp(num1,num2);
		if ((num1.minus&&num2.minus&&c>0)||(!num1.minus&&!num2.minus&&c<0))
			res.minus=true;								//����������� ����� ����������

		if (c>0)										//�� �������� (�� ������) �������� ������� (��� ��� �������������)
		{
			int d2;										//d2 - ��������������� ����������, �������� "�����" ������� ����� ��� 0, ���� ��� �����������
			res.amount=num1.amount;
			res.digit= (int*)calloc(res.amount, sizeof(int));
			int i;
			for (i=0; i<res.amount; i++)
			{
				if (i>=num2.amount) d2=0;
				else d2=num2.digit[i];
				res.digit[i]=num1.digit[i]-d2;
				if (res.digit[i]<0)						//���
				{
					res.digit[i]+=osn;
					num1.digit[i+1]--;
				}
			}
		}
		else
		{
			int d1;										//d1 - ��������������� ����������, �������� "�����" ������� ����� ��� 0, ���� ��� �����������
			res.amount=num2.amount;
			res.digit= (int*)calloc(res.amount, sizeof(int));
			int i;
			for (i=0; i<res.amount; i++)
			{
				if (i>=num1.amount) d1=0;
				else d1=num1.digit[i];
				res.digit[i]=num2.digit[i]-d1;
				if (res.digit[i]<0)						//���
				{
					res.digit[i]+=osn;
					num2.digit[i+1]--;						
				}
			}
		}
														//������� ������ ���� � ������� ��������
		c=res.amount-1;
		while(res.digit[c]==0)
			c--;
		if (c!=res.amount-1)
		{
			res.amount=c+1;
			res.digit=(int*)realloc(res.digit, res.amount*sizeof(int));
		}
	}
	return res;
}

//======================================== ��������� �������� �� ������� ===============================================
BigInt BI_mul(BigInt num1, BigInt num2)
{
	BigInt res;											//����������� ����� ����������
	if (num1.minus==num2.minus)
		res.minus=false;
	else res.minus=true;

	res.amount=num1.amount+num2.amount;					//���������� "����" � ����������
	res.digit=(int*)calloc(res.amount, sizeof(int));
	int i, j, r;
	for (i=0; i<num1.amount; i++)
	{
		r=0;
		for (j=0; j<num2.amount; j++)					//��������� ������ "�����" ������� �� ������ "�����"�������
		{
			res.digit[i+j]+=num1.digit[i]*num2.digit[j];
			r=res.digit[i+j]/osn;
			res.digit[i+j] -= r*osn;					//������ ������������
			res.digit[i+j+1]+=r;
		}
	}
														//�������� ������ ����� � ������� ��������
	r=res.amount-1;
	while(res.digit[r]==0)
		r--;
	if (r!=res.amount-1)
	{
		res.amount=r+1;
		res.digit=(int*)realloc(res.digit, res.amount*sizeof(int));
	}
	
	return res;
}

//======================================== ��������� �������� �� ����� ��� ����� ����� ============================================
BigInt BI_int_mul(BigInt num1, int n)
{														//���������� ������� - ����������� � ������ ���������
	BigInt res;											//���������� �� ���� ��������� �� �������
	res.minus=false;
	res.amount=num1.amount;
	res.digit=(int*)calloc(res.amount, sizeof(int));
	int j, r=0;
	for (j=0; j<num1.amount; j++)
	{
		res.digit[j]+=num1.digit[j]*n;
		r=res.digit[j]/osn;
		res.digit[j] -= r*osn;
		if ((j==num1.amount-1)&&(r!=0))
		{
			res.amount++;
			res.digit=(int*)realloc(res.digit, res.amount*sizeof(int));
			res.digit[j+1]=0;
		}
		res.digit[j+1]+=r;
	}

	return res;
}

//======================================== ������� ===============================================
BigInt BI_div(BigInt num1, BigInt num2)
{
	BigInt res;		
	if (num2.amount==1&&num2.digit[0]==0)				//��� ������� �� 0 ������ ������ � �������� � ��������� 0
	{
		printf("division by zero!\n");
		res.amount=1;
		res.digit=(int*)calloc(res.amount, sizeof(int));
	}
	else												//����� ����� ����������� ��������
	{
		if (num1.minus==num2.minus)						//���������� ���� ����������
			res.minus=false;
		else res.minus=true;

		if (BI_comp(num1, num2)<0)						//���� ������� �� ������ ������ ��������, �� ��������� 0
		{	
			res.amount=1;
			res.digit=(int*)calloc(res.amount, sizeof(int));
		}
		else											//����� ����� ������ ������� (��� �������������, �� ��������� "� �������")
		{
			res.amount=num1.amount;						//��������� ������ � ���������� = � ��������
			res.digit=(int*)calloc(res.amount,sizeof(int));
			BigInt cur, tmp, tmp2;
			cur.minus=false;							//cur - ������� "�������", ���������� ������ = � �������� +�� 1
			cur.amount=num2.amount;
			cur.digit=(int*)malloc(cur.amount*sizeof(int));
			int i1, ires, icur, x, l, r, m;
			i1=num1.amount-num2.amount; 
			for (icur=0; icur<cur.amount; icur++)
				cur.digit[icur]=num1.digit[i1+icur];
			ires=res.amount-1;							//ires - ������ ������� "�����" �������� - ������� �� �������� ��������, �.�. � �����
			
			while (1)
			{
				if (BI_comp(num2, cur)>0)
				{										//���� cur<num2, �������� cur*osn � ���������� ��� ���� ����� �� num1
					cur.amount++;
					cur.digit = (int*)realloc(cur.digit, cur.amount*sizeof(int));
					for (icur=cur.amount-2; icur>=0; icur--)
						cur.digit[icur+1]=cur.digit[icur];
					i1--;
					cur.digit[0]=num1.digit[i1];
				}

				x=0; l=0; r=osn;						//������ ���������� "�����" x: x*num2<=cur ������� ����������� �������
				while(l<=r)
				{
					m=(l+r)/2;
					tmp = BI_int_mul(num2, m);
					if (BI_comp(tmp, cur)<=0)
					{	x=m; l=m+1;}
					else r=m-1;
				}
				res.digit[ires]=x;						//�������� � � ���������
				ires--;
														//cur -= x*num2
														//��������� �������������� ���������� tmp, tmp2
				tmp = BI_int_mul(num2, x);				
	//fprintf(f, "\ncur=");
	//BI_output(cur,f);
	//fprintf(f, "\nx=%d\ntmp=", x);
	//BI_output(tmp,f);
				tmp2=BI_sub(cur,tmp);
				cur.amount=tmp2.amount;
				cur.digit=(int*)malloc(cur.amount*sizeof(int));
				int r;
				for (r=0; r<cur.amount; r++)
					cur.digit[r]=tmp2.digit[r];
				free(tmp2.digit);

	//fprintf(f, "\ncur=cur-tmp=");
	//BI_output(cur,f);

				if (i1==0)								//���� ��������� ����� num1 ��� ������������ - ������� ���������
					break;
														//���� cur!=0, �������� cur*osn, ����� ���������� ��� ���� ����� �� num1
				if (!((cur.amount==1)&&(cur.digit[0]==0)))
				{
					cur.amount++;
					cur.digit = (int*)realloc(cur.digit, cur.amount*sizeof(int));
					for (icur=cur.amount-1; icur>=0; icur--)
						cur.digit[icur+1]=cur.digit[icur];
				}
				i1--;
				cur.digit[0]=num1.digit[i1];
			}
			
														//������� ������ ���� � ������ �������� ������ res
			if (ires>=0)
			{
				ires++;
				res.amount-=ires;
				int i;
				for (i=0; i<res.amount; i++)
					res.digit[i]=res.digit[i+ires];
				res.digit=(int*)realloc(res.digit, res.amount*sizeof(int));
			}

			free(cur.digit);
			free(tmp.digit);
		}
	}
//fprintf(f, "\n");
	return res;
}

//======================================== ������� �� ������� ===============================================
BigInt BI_mod(BigInt num1, BigInt num2)
{														//���������� � �������� ��� �������, ������ ��������� � ����� � cur
	BigInt cur;
	int r=BI_comp(num1, num2);
	if (r<0)
	{													//���� num1<num2, �� res=num1
		cur.minus=num1.minus;
		cur.amount=num1.amount;
		cur.digit=(int*)malloc(cur.amount*sizeof(int));
		int i;
		for (i=0; i<cur.amount; i++)
			cur.digit[i]=num1.digit[i];
	}
	else if (num2.amount==1&&num2.digit[0]==0)			//���� num2=0, �� ������ ������� �� 0, � res=0
	{
		printf("division by zero!\n");
		cur.amount=1;
		cur.digit=(int*)calloc(cur.amount, sizeof(int));
	}
	else												//����� ����� ����������� ��������
	{
		BigInt tmp, tmp2;
		cur.minus=num2.minus;							//��������� ����� � cur!
		cur.amount=num2.amount;							//cur - ������� "�������", ���������� ������ = � �������� +�� 1
		cur.digit=(int*)malloc(cur.amount*sizeof(int));
		int i1, ires, icur, x, l, r, m;
		i1=num1.amount-num2.amount; 
		for (icur=0; icur<cur.amount; icur++)
			cur.digit[icur]=num1.digit[i1+icur];
		
		while (1)
		{
			if (BI_comp(num2, cur)>0)
			{											//���� cur<num2, �������� cur*osn � ���������� ��� ���� ����� �� num1
				cur.amount++;
				cur.digit = (int*)realloc(cur.digit, cur.amount*sizeof(int));
				for (icur=cur.amount-2; icur>=0; icur--)
					cur.digit[icur+1]=cur.digit[icur];
				i1--;
				cur.digit[0]=num1.digit[i1];
			}

			x=0; l=0; r=osn;							//������ ���������� "�����" x: x*num2<=cur ������� ����������� �������
			while(l<=r)
			{
				m=(l+r)/2;
				tmp = BI_int_mul(num2, m);
				if (BI_comp(tmp, cur)<=0)
				{	x=m; l=m+1;}
				else r=m-1;
			}
														//cur -= x*num2
														//��������� �������������� ���������� tmp, tmp2
			tmp = BI_int_mul(num2, x);
			tmp2=BI_sub(cur,tmp);
			cur.amount=tmp2.amount;
			cur.digit=(int*)malloc(cur.amount*sizeof(int));
			int r;
			for (r=0; r<cur.amount; r++)
				cur.digit[r]=tmp2.digit[r];
			free(tmp2.digit);

			if (i1==0)									//���� ��������� ����� num1 ��� ������������ - ����� �� �����
				break;
														//���� |cur!=0, �������� cur*osn, ����� ���������� ��� ���� ����� �� num1
			if (!((cur.amount==1)&&(cur.digit[0]==0)))
			{
				cur.amount++;
				cur.digit = (int*)realloc(cur.digit, cur.amount*sizeof(int));
				for (icur=cur.amount-1; icur>=0; icur--)
					cur.digit[icur+1]=cur.digit[icur];
			}
			i1--;
			cur.digit[0]=num1.digit[i1];

			free(tmp.digit);
		}
	}
	return cur;
}

//======================================== ���������� � ������� �� ������ ===============================================
BigInt BI_deg(BigInt num, BigInt deg, BigInt mod)
{														//���������! ������ �������� num,deg
	BigInt res, two, one, zero;
	two.amount=1;										//����������� ����� 0,1,2 � ������� �������
	two.digit=(int*)malloc(two.amount*sizeof(int));
	two.digit[0]=2;
	two.minus=false;
	one.amount=1;
	one.digit=(int*)malloc(one.amount*sizeof(int));
	one.digit[0]=1;
	one.minus=false;
	zero.amount=1;
	zero.digit=(int*)malloc(zero.amount*sizeof(int));
	zero.digit[0]=0;
	zero.minus=false;
	res.amount=1;
	res.digit=(int*)malloc(res.amount*sizeof(int));
	res.digit[0]=1;
	res.minus=false;


	while (BI_comp(deg, zero)>0)						//���������� � ������� - ����������, � ������������ � �������� ������� deg
	{
		if (BI_comp(BI_mod(deg, two), one)==0)
		{
			res=BI_mod(BI_mul(res, num), mod);
		}
		num=BI_mod(BI_mul(num, num), mod);
		deg=BI_div(deg, two);
	}

	free(zero.digit);
	free(one.digit);
	free(two.digit);
	return res;
}



//                       ******************************************
//                       *                                        *
//                       *   ������� ������ � ��������� �������   *
//                       *                                        *
//                       ******************************************
//������ � �������� ������� ������������ ��������������� � ������, � �� ������ �� �����������
//������ ��������� ����� little-endian
//��� �������� �����������
//�� ���� �������������� ��������� �� ���� ������� �������� �����(!) ������, � �� ���������, 
//��������-�������� ���������� ������ ������ ������� ��������
//������������ ����� ������������ �����

//=========================================== ��������� ===============================================
int BI_comp_b(FILE *num1, FILE *num2)
{														//�� ����� ��� �������� ����� � �������
	int len1, len2;
	fseek(num1, 0, SEEK_END);
	len1=ftell(num1);
	fseek(num2, 0, SEEK_END);
	len2=ftell(num2);
	int i;
	unsigned char d1, d2;
	i=len1-1;
	fseek(num1, i*sizeof_base, 0);
	fread(&d1, sizeof_base, 1, num1);
	while(d1==0&&i>=0)									//� d1 ����������� ����� ������� �� ������� ��������(� ����� �����)
	{													//������������� ���������� ����
		i--;
		fseek(num1, i*sizeof_base, 0);
		fread(&d1, sizeof_base, 1, num1);
	}
	len1=i+1;
	i=len2-1;
	fseek(num2, i*sizeof_base, 0);
	fread(&d1, sizeof_base, 1, num2);
	while(d1==0&&i>=0)									//����������, ������������� ���� � ������� ��������
	{
		i--;
		fseek(num2, i*sizeof_base, 0);
		fread(&d1, sizeof_base, 1, num2);
	}
	len2=i+1;

//printf("\nBI_comp_b: len1=%d len2=%d\n", len1, len2);
	if (len1>len2)										//������� ������������ �����
		return 1;
	if (len1<len2)
		return -1;
	for (i=len1-1; i>=0; i--)							//��� ������ ����� ���������� ��������� ���������, �� ������� � �������, �� ������� ��������
	{
		fseek(num1, i*sizeof_base, 0);
		fread(&d1, sizeof_base, 1, num1);
		fseek(num2, i*sizeof_base, 0);
		fread(&d2, sizeof_base, 1, num2);
		if (d1>d2)
			return 1;
		if (d1<d2)
			return -1;
	}
	return 0;
}

//======================================== �������� (��������) ===============================================
int BI_add_b(char *filenum1, char *filenum2, char *fileres)
{														//�������� ������
	FILE *num1, *num2, *res;
	if ((num1 = fopen(filenum1, "rb"))==NULL)
	{
		perror("Can not open file #1");
		return 11;
	}
	if ((num2 = fopen(filenum2, "rb"))==NULL)
	{
		perror("Can not open file #2");
		return 12;
	}
	if ((res = fopen(fileres, "wb"))==NULL)
	{
		perror("Can not open file #3");
		return 13;
	}
														
	int i=0, j=0, k=0, r=0, res_int;
	unsigned char d1, d2, res_ch;
	int len1, len2;										//����������� ����� ������
	fseek(num1, 0, SEEK_END);
	len1=ftell(num1);
	fseek(num2, 0, SEEK_END);
	len2=ftell(num2);

	while (1)											//���������� ��������
	{													//� r - �������
		if (i>=len1&&j>=len2)							//���� ��� ��������� ����� � r>0, �������� r � ���������
		{
			if (r==1)
			{
				res_ch=1;
				fseek(res, k*sizeof_base, 0);
				fwrite(&res_ch, sizeof_base, 1, res);
//printf("res[%d]: %d\n", k, res_ch);
			}
			break;
		}
		if (i>=len1) d1=0;								//d1 - ������� ����� num1 ��� 0, ���� num1 �����������
		else 
		{
			fseek(num1, i*sizeof_base, 0);
			fread(&d1, sizeof_base, 1, num1);
			i++;
		}
//printf("num1[%d]: %d\n", i-1, d1);
		if (j>=len2) d2=0;								//d2 - ������� ����� num2 ��� 0, ���� num2 �����������
		else 
		{
			fseek(num2, j*sizeof_base, 0);
			fread(&d2, sizeof_base, 1, num2);
			j++;
		}
//printf("num2[%d]: %d\n", j-1, d2);

		res_int=d1+d2+r;								//�������� � ������ ��������
		if (res_int>=base)
		{
			res_ch=res_int-base;
			r=1;
		}
		else
		{
			res_ch=res_int;
			r=0;
		}

		fseek(res, k*sizeof_base, 0);					//������ ������� ����� � ���� ����������
		fwrite(&res_ch, sizeof_base, 1, res);
		k++;
//printf("res[%d]: %d\n", k-1, res_int);
	}
														//�������� ������
	fclose(num1);
	fclose(num2);
	fclose(res);
	return 0;
}

//======================================== ��������� (�����������) ===============================================
int BI_sub_b(char *filenum1, char *filenum2, char *fileres)
{														//�������� ������
	FILE *num1, *num2, *res;
	if ((num1 = fopen(filenum1, "rb"))==NULL)
	{
		perror("Can not open file #1");
		return 11;
	}
	if ((num2 = fopen(filenum2, "rb"))==NULL)
	{
		perror("Can not open file #2");
		return 12;
	}
	if ((res = fopen(fileres, "wb"))==NULL)
	{
		perror("Can not open file #3");
		return 13;
	}

	int c=BI_comp_b(num1,num2);							//���� �� ������ num1<num2
	int len1, len2;
	fseek(num1, 0, SEEK_END);
	len1=ftell(num1);
	fseek(num2, 0, SEEK_END);
	len2=ftell(num2);

	unsigned char d1, d2, res_ch, r=0;
	int i, res_int;

	if (c<0)											//�� ����� �������� �� num2 num1, �� ���� �������� �������
	{
		FILE *temp;
		temp=num1;
		num1=num2;
		num2=temp;
	}
	for (i=0; i<len1; i++)								//������ ���������
	{
		if (i>=len2) d2=0;								//d2 - ������� ����� num2 ��� 0, ���� num2 �����������
		else 
		{
			fseek(num2, i*sizeof_base, 0);
			fread(&d2, sizeof_base, 1, num2);
		}
		fseek(num1, i*sizeof_base, 0);
		fread(&d1, sizeof_base, 1, num1);				//d1 - ������� ����� num1

		res_int=d1-d2-r;								//��������� � ������ ��������
		if (res_int<0)
		{
			res_ch=res_int+base;
			r=1;
		}
		else 
		{
			r=0;
			res_ch=res_int;
		}
		fseek(res, i*sizeof_base, 0);					//������ ������� ������ � ��� ����������
		fwrite(&res_ch, sizeof_base, 1, res);
	}
														//�������� ������
	fclose(num1);
	fclose(num2);
	fclose(res);
	return 0;
}

//======================================== ��������� �������� �� ������� ===============================================
int BI_mul_b(char *filenum1, char *filenum2, char *fileres)
{														//�������� ������
	FILE *num1, *num2, *res;
	if ((num1 = fopen(filenum1, "rb"))==NULL)
	{
		perror("Can not open file #1");
		return 1;
	}
	if ((num2 = fopen(filenum2, "rb"))==NULL)
	{
		perror("Can not open file #2");
		return 2;
	}
	if ((res = fopen(fileres, "wb+"))==NULL)
	{
		perror("Can not open file #3");
		return 3;
	}

	int i, j, k, r, res_int, tmp;
	unsigned char d1, d2, res_ch;
	int len1, len2;										//len1, len2 - ����� num1, num2
	fseek(num1, 0, SEEK_END);
	len1=ftell(num1);
	fseek(num2, 0, SEEK_END);
	len2=ftell(num2);

	for (i=0; i<len1; i++)								//��������� ������ ����� num1 �� ������ ����� num2
	{
		fseek(num1, i*sizeof_base, 0);					//d1 - ������� ����� num1
		fread(&d1, sizeof_base, 1, num1);
		for (j=0; j<len2; j++)
		{
			fseek(num2, j*sizeof_base, 0);				//d2 - ������� ����� num2
			fread(&d2, sizeof_base, 1, num2);
//printf("\nnum1[%d]: %x\nnum2[%d]: %x\n", i, d1, j, d2);
			tmp=d1*d2;	
			r = tmp/(int)(base);						//r - ������� � ��������� ������
			tmp-= r*base;
//printf("r: %x\ntmp: %x\n", r, tmp);
			k=i+j;										//k - ������� � ����� ����������
			fseek(res, k*sizeof_base, 0);
			if (!fread(&res_int, sizeof_base, 1, res)>0) res_int=0;
//printf("res[%d]: %x\n", k, res_int);
			res_int += tmp;
			if (res_int>=base)
			{
				r++;
				res_int-=base;
			}
			res_ch=(unsigned char)res_int;
			fseek(res, k*sizeof_base, 0);				//������ ������� ����� � ���� ����������
			if (!fwrite(&res_ch, sizeof_base, 1, res)>0) printf("Fwrite Error!\n");
//printf("res[%d]: %x\n", k, res_ch);

			while (r>0)									//������ �������� � ��������� �������
			{
				k++;
				res_int=0;
				fseek(res, k*sizeof_base, 0);
				if (!fread(&res_int, sizeof_base, 1, res)>0) res_int=0;
//printf("res[%d]: %x\n", k, res_int);
//printf("r: %x\n", r);
				res_int+=r;
				if (res_int>=base)
				{
					r=1;
					res_int-=base;
				}
				else r=0;
				res_ch=(unsigned char)res_int;
				fseek(res, k*sizeof_base, 0);
				if (!fwrite(&res_ch, sizeof_base, 1, res)>0) printf("Fwrite2 Error!\n");
//printf("res[%d]: %x\n", k, res_ch);
			}
		}
	}
														//�������� ������
	fclose(num1);
	fclose(num2);
	fclose(res);
	return 0;
}

//======================================== ��������� �������� �� ����� ============================================
int BI_int_mul_b(FILE *num1, unsigned char n, FILE *res)
{														//��������� �������, ����� ��� ������ ��������
														//�� ����� - ��� ��������� ��� �������� ������ � �������
	int i, j, k, r=0, res_int, tmp, len=0;
	unsigned char d1, res_ch;
	int len1, lenr;										//len1, lenr - ����� ������ num1 � ����������
	fseek(num1, 0, SEEK_END);
	len1=ftell(num1);

	fseek(res, 0, SEEK_END);
	lenr=ftell(res);
	for (i=0; i<lenr; i++)								//���������� � ��������� ��� ���� ��� ������
	{
		d1=0;
		fseek(res, i*sizeof_base, SEEK_SET);
		fwrite(&d1, sizeof_base, 1, res);
	}


//printf("\nBI_int_mul_b: \nnum1=");
//for (i=0; i<len1; i++)
//{
//	fseek(num1, i*sizeof_base, SEEK_SET);
//	fread(&d1, sizeof_base, 1, num1);
//	printf("%d ", d1);
//}
//printf("\nres=");
//for (i=0; i<lenr; i++)
//{
//	fseek(res, i*sizeof_base, SEEK_SET);
//	fread(&d1, sizeof_base, 1, res);
//	printf("%d ", d1);
//}
//
//printf("\nn=%d\n", n);

	for (j=0; j<len1; j++)								//��� ������ ����� d1 ����� num1 (������� � �������)
	{
		fseek(num1, j*sizeof_base, 0);
		fread(&d1, sizeof_base, 1, num1);
		tmp=d1*(int)n;									//�������� d1*n
//printf("tmp=d1*n= %d  ", tmp);					
		r=tmp/(int)base;								//� r - �������
		tmp-=r*(int)base;
//printf("r=%d  tmp=%d  ", r,tmp);
		fseek(res, j*sizeof_base, 0);					//���������� � ������� ����� ���������� ���������� ��������
		if (!fread(&res_int, sizeof_base, 1, res)>0) res_int=0;
		res_int+=tmp;					
		if (res_int>=base)								
		{
			r++;
			res_int-=base;
		}
		res_ch=(unsigned char)res_int;
		fseek(res, j*sizeof_base, 0);
		if (!fwrite(&res_ch, sizeof_base, 1, res)>0) printf("Write Error!\n");
//printf("res[%d]=%d\n", j, res_ch);
		k=j;
		while (r>0)										//��������� ��������
		{
			k++;
			fseek(res, k*sizeof_base, 0);
			if (!fread(&res_ch, sizeof_base, 1, res)>0) res_ch=0;
			res_int=(int)res_ch+r;
			if (res_int>=base)
			{
				r=1;
				res_int-=base;
			}
			else r=0;
			res_ch=(unsigned char)res_int;
			fseek(res, k*sizeof_base, 0);
			if (!fwrite(&res_ch, sizeof_base, 1, res)>0) printf("WriteError2!\n");
//printf("res[%d]=%d\n", k, res_ch);
		}
		if (k>len) len=k;
	}
//fseek(res, 0, SEEK_END);
//len1=ftell(res);
//
//printf("SO, res=");
//for (i=0; i<len1; i++)
//{
//	fseek(res, i*sizeof_base, SEEK_SET);
//	fread(&d1, sizeof_base, 1, res);
//	printf("%d ", d1);
//}
//printf("\n");
	return len;
}

//======================================== ������� ===============================================
int BI_div_b(char *filenum1, char *filenum2, char *fileres)
{														//�������� ������
	FILE *num1, *num2, *res;
	if ((num1 = fopen(filenum1, "rb"))==NULL)
	{
		perror("Can not open file #1");
		return 1;
	}
	if ((num2 = fopen(filenum2, "rb"))==NULL)
	{
		perror("Can not open file #2");
		return 2;
	}
	if ((res = fopen(fileres, "wb+"))==NULL)
	{
		perror("Can not open file #3");
		return 3;
	}


	if (BI_comp_b(num1, num2)<0)						//���� ������� ������ ��������, �� � ���������� 0
	{
		unsigned char d1=0;
		fseek(res, 0*sizeof_base, SEEK_SET);
		fwrite(&d1, sizeof_base, 1, res);
	}
	else												//����� ����� ����������� ������
	{
		FILE *cur, *tmp, *tmp2;							//�������������� �����, ����������
		cur=fopen("dopfile1", "wb+");
		tmp=fopen("dopfile2", "wb+");
		tmp2=fopen("dopfile3", "wb+");

		int i1, ires, icur, l, r, lenr, i;
		unsigned char x, m;
		unsigned char d1, d2;
		int len1, len2, lenc;
		fseek(num1, 0, SEEK_END);
		len1=ftell(num1);
		fseek(num2, 0, SEEK_END);
		len2=ftell(num2);
//printf("len1=%d len2=%d\n", len1, len2);
		i1=len1-len2; 
		for (icur=0; icur<len2; icur++)
		{												//cur �������� ������� "�������", ������� ��� ������ len2 ������ �� num1
			fseek(num1, (i1+icur)*sizeof_base, SEEK_SET);
			fread(&d1, sizeof_base, 1, num1);
			fseek(cur, icur*sizeof_base, SEEK_SET);
			fwrite(&d1, sizeof_base, 1, cur);
		}
		ires=lenr=len1-1;
		
//int j;
//printf("START\nnum1: ");
//fseek(num1, 0, SEEK_END);
//j=ftell(num1);
//for (i=0; i<j; i++)
//{
//	fseek(num1, i*sizeof_base, SEEK_SET);
//	fread(&d1, sizeof_base, 1, num1);
//	printf("%x ", d1);
//}
//printf("\n");
//printf("num2: ");
//fseek(num2, 0, SEEK_END);
//j=ftell(num2);
//for (i=0; i<j; i++)
//{
//	fseek(num2, i*sizeof_base, SEEK_SET);
//	fread(&d1, sizeof_base, 1, num2);
//	printf("%x ", d1);
//}
//printf("\n");
//printf("cur: ");
//fseek(cur, 0, SEEK_END);
//j=ftell(cur);
//for (i=0; i<j; i++)
//{
//	fseek(cur, i*sizeof_base, SEEK_SET);
//	fread(&d1, sizeof_base, 1, cur);
//	printf("%x ", d1);
//}
//printf("\n");


		while (i1>=0)
		{
			if (BI_comp_b(num2, cur)>0)
			{
														//���� cur<num2, �������� cur*osn � ���������� ��� ���� ����� �� num1
				fseek(cur, 0, SEEK_END);
				lenc=ftell(cur);
				r=0;
				for (i=lenc-1; i>=0; i--)
				{
					fseek(cur, i*sizeof_base, SEEK_SET);
					fread(&d1, sizeof_base, 1, cur);
					if (d1!=0) {r=1; break;}
				}
				if (r==1)
				{
					lenc=i+1;
					lenc++;
					for (icur=lenc-2; icur>=0; icur--)
					{
						fseek(cur, icur*sizeof_base, SEEK_SET);
						fread(&d1, sizeof_base, 1, cur);
						fseek(cur, (icur+1)*sizeof_base, SEEK_SET);
						fwrite(&d1, sizeof_base, 1, cur);
					}
				}
//printf("i1=%d\n", i1);
				i1--;
				fseek(num1, i1*sizeof_base, SEEK_SET);
				fread(&d1, sizeof_base, 1, num1);
				fseek(cur, 0*sizeof_base, SEEK_SET);
				fwrite(&d1, sizeof_base, 1, cur);
//printf("cur: ");
//fseek(cur, 0, SEEK_END);
//j=ftell(cur);
//for (i=0; i<j; i++)
//{
//	fseek(cur, i*sizeof_base, SEEK_SET);
//	fread(&d1, sizeof_base, 1, cur);
//	printf("%x ", d1);
//}
//printf("\n");
			}

			int len;
			x=0; l=0; r=base;							//������ ���������� "�����" x: x*num2<=cur ������� ����������� �������		
			while(l<=r)
			{
				if (l==(int)base&&r==(int)base) break;
				m=(l+r)/2;
//printf("l=%d m=%d r=%d\n", l, m, r);
				len=BI_int_mul_b(num2, m, tmp);
				fseek(tmp, 0, SEEK_END);
				lenc=ftell(tmp);
				if (lenc>len)
					for (i=len; i<lenc; i++)
					{
						d1=0;
						fseek(tmp, i*sizeof_base, SEEK_SET);
						fread(&d1, sizeof_base, 1, tmp);
					}
				if (BI_comp_b(tmp, cur)<=0)
				{	x=m; l=m+1;}
				else r=m-1;
			}
			fseek(res, ires*sizeof_base, SEEK_SET);		//���������� ��������� ����� � ���������
			fwrite(&x, sizeof_base, 1, res);
//printf("x=%x\n", x);
			ires--;
														//cur -= x*num2
			BI_int_mul_b(num2, x, tmp);
			fseek(tmp, 0, SEEK_END);
			lenc=ftell(tmp);
			if (lenc>len)
				for (i=len; i<lenc; i++)
				{
					d1=0;
					fseek(tmp, i*sizeof_base, SEEK_SET);
					fread(&d1, sizeof_base, 1, tmp);
				}

			BI_sub_b("dopfile1", "dopfile2", "dopfile3");


//printf("tmp=x*num2: ");
//fseek(tmp, 0, SEEK_END);
//lenc=ftell(tmp);
//for (i=0; i<lenc; i++)
//{
//	fseek(tmp, i*sizeof_base, SEEK_SET);
//	fread(&d1, sizeof_base, 1, tmp);
//	printf("%x ", d1);
//}
//printf("\n");
//fseek(tmp2, 0, SEEK_END);
//lenc=ftell(tmp2);
//printf("cur-tmp: ");
//for (i=0; i<lenc; i++)
//{
//	fseek(tmp2, i*sizeof_base, SEEK_SET);
//	fread(&d1, sizeof_base, 1, tmp2);
//	printf("%x ", d1);
//}
//printf("\n");

			fseek(tmp2, 0, SEEK_END);
			lenc=ftell(tmp2);
			int r;
			for (r=0; r<lenc; r++)
			{
				fseek(tmp2, r*sizeof_base, SEEK_SET);
				fread(&d1, sizeof_base, 1, tmp2);
				fseek(cur, r*sizeof_base, SEEK_SET);
				fwrite(&d1, sizeof_base, 1, cur);
			}

			if (i1<=0)									//���� ��������� ����� num1 ��� ������������ - ����� �� �����
				break;

		}
		
														//������� ������ ���� � ������ �������� ������ res
		if (ires>=0)
		{
			ires++;
			lenr++;
			int real_len=lenr-ires;
			for (r=0; r<lenr; r++)
			{
				if (r<real_len)
				{
					fseek(res, (r+ires)*sizeof_base, SEEK_SET);
					fread(&d1, sizeof_base, 1, res);
				}
				else d1=0;
				fseek(res, r*sizeof_base, SEEK_SET);
				fwrite(&d1, sizeof_base, 1, res);
			}
		}
														//�������� ������������� ������
		fclose(cur); remove("dopfile1");
		fclose(tmp); remove("dopfile2");
		fclose(tmp2); remove("dopfile3");

	}
														//�������� ������
	fclose(num1);
	fclose(num2);
	fclose(res);
	return 0;
}

//======================================== ������� �� ������� ===============================================
int BI_mod_b(char *filenum1, char *filenum2, char *fileres)
{
	FILE *num1, *num2, *cur;
	if ((num1 = fopen(filenum1, "rb"))==NULL)
	{
		perror("Can not open file #1");
		return 1;
	}
	if ((num2 = fopen(filenum2, "rb"))==NULL)
	{
		perror("Can not open file #2");
		return 2;
	}
	if ((cur = fopen(fileres, "wb+"))==NULL)
	{
		perror("Can not open file #3");
		return 3;
	}


	if (BI_comp_b(num1, num2)<0) 
	{
		int i, len1;
		unsigned char d1;
		fseek(num1, 0, SEEK_END);
		len1=ftell(num1);
		for (i=0; i<len1; i++)
		{
			fseek(num1, i*sizeof_base, SEEK_SET);
			fread(&d1, sizeof_base, 1, num1);
			fseek(cur, i*sizeof_base, SEEK_SET);
			fwrite(&d1, sizeof_base, 1, cur);
		}
	}
	else
	{
		FILE *tmp, *tmp2;
		tmp=fopen("dopfile2", "wb+");
		tmp2=fopen("dopfile3", "wb+");

		int i1, ires, icur, l, r, lenr, i;
		unsigned char x, m;
		unsigned char d1, d2;
		int len1, len2, lenc;
		fseek(num1, 0, SEEK_END);
		len1=ftell(num1);
		fseek(num2, 0, SEEK_END);
		len2=ftell(num2);
//printf("len1=%d len2=%d\n", len1, len2);
		i1=len1-len2; 
		for (icur=0; icur<len2; icur++)
		{
			fseek(num1, (i1+icur)*sizeof_base, SEEK_SET);
			fread(&d1, sizeof_base, 1, num1);
			fseek(cur, icur*sizeof_base, SEEK_SET);
			fwrite(&d1, sizeof_base, 1, cur);
		}
		ires=lenr=len1-1;
		
//int j;
//printf("START\nnum1: ");
//fseek(num1, 0, SEEK_END);
//j=ftell(num1);
//for (i=0; i<j; i++)
//{
//	fseek(num1, i*sizeof_base, SEEK_SET);
//	fread(&d1, sizeof_base, 1, num1);
//	printf("%x ", d1);
//}
//printf("\n");
//printf("num2: ");
//fseek(num2, 0, SEEK_END);
//j=ftell(num2);
//for (i=0; i<j; i++)
//{
//	fseek(num2, i*sizeof_base, SEEK_SET);
//	fread(&d1, sizeof_base, 1, num2);
//	printf("%x ", d1);
//}
//printf("\n");
//printf("cur: ");
//fseek(cur, 0, SEEK_END);
//j=ftell(cur);
//for (i=0; i<j; i++)
//{
//	fseek(cur, i*sizeof_base, SEEK_SET);
//	fread(&d1, sizeof_base, 1, cur);
//	printf("%x ", d1);
//}
//printf("\n");


		while (1)
		{
			if (BI_comp_b(num2, cur)>0)
			{
				//���� cur<num2 � cur!=0, �������� cur*osn � ���������� ��� ���� ����� �� num1
				fseek(cur, 0, SEEK_END);
				lenc=ftell(cur);
				r=0;
				for (i=lenc-1; i>=0; i--)
				{
					fseek(cur, i*sizeof_base, SEEK_SET);
					fread(&d1, sizeof_base, 1, cur);
					if (d1!=0) {r=1; break;}
				}
				if (r==1)
				{
					lenc=i+1;
					lenc++;
					for (icur=lenc-2; icur>=0; icur--)
					{
						fseek(cur, icur*sizeof_base, SEEK_SET);
						fread(&d1, sizeof_base, 1, cur);
						fseek(cur, (icur+1)*sizeof_base, SEEK_SET);
						fwrite(&d1, sizeof_base, 1, cur);
					}
				}
				i1--;
				fseek(num1, i1*sizeof_base, SEEK_SET);
				fread(&d1, sizeof_base, 1, num1);
				fseek(cur, 0*sizeof_base, SEEK_SET);
				fwrite(&d1, sizeof_base, 1, cur);
			}

//printf("cur: ");
//fseek(cur, 0, SEEK_END);
//int j=ftell(cur);
//for (i=0; i<j; i++)
//{
//	fseek(cur, i*sizeof_base, SEEK_SET);
//	fread(&d1, sizeof_base, 1, cur);
//	printf("%x ", d1);
//}
//printf("\n");

			int len;
			x=0; l=0; r=(int)base;
			while(l<=r)
			{
				if (l==(int)base&&r==(int)base) break;
				m=(unsigned char)((l+r)/2);
//printf("l=%x m=%x r=%x\n", l, m, r);
				len=BI_int_mul_b(num2, m, tmp);
				fseek(tmp, 0, SEEK_END);
				lenc=ftell(tmp);
				if (lenc>len)
					for (i=len; i<lenc; i++)
					{
						d1=0;
						fseek(tmp, i*sizeof_base, SEEK_SET);
						fread(&d1, sizeof_base, 1, tmp);
					}
				if (BI_comp_b(tmp, cur)<=0)
				{	x=m; l=(int)m+1;}
				else r=(int)m-1;
			}
			//fseek(res, ires*sizeof_base, SEEK_SET);
			//fwrite(&x, sizeof_base, 1, res);
//printf("x=%x\n", x);
			//ires--;

			BI_int_mul_b(num2, x, tmp);
			fseek(tmp, 0, SEEK_END);
			lenc=ftell(tmp);
			if (lenc>len)
				for (i=len; i<lenc; i++)
				{
					d1=0;
					fseek(tmp, i*sizeof_base, SEEK_SET);
					fread(&d1, sizeof_base, 1, tmp);
				}

			//tmp2 = cur - tmp;
			BI_sub_b(fileres, "dopfile2", "dopfile3");


//printf("tmp=x*num2: ");
//fseek(tmp, 0, SEEK_END);
//lenc=ftell(tmp);
//for (i=0; i<lenc; i++)
//{
//	fseek(tmp, i*sizeof_base, SEEK_SET);
//	fread(&d1, sizeof_base, 1, tmp);
//	printf("%x ", d1);
//}
//printf("\n");
//fseek(tmp2, 0, SEEK_END);
//lenc=ftell(tmp2);
//printf("cur-tmp: ");
//for (i=0; i<lenc; i++)
//{
//	fseek(tmp2, i*sizeof_base, SEEK_SET);
//	fread(&d1, sizeof_base, 1, tmp2);
//	printf("%x ", d1);
//}
//printf("\n");

			fseek(tmp2, 0, SEEK_END);
			lenc=ftell(tmp2);
			int r;
			for (r=0; r<lenc; r++)
			{
				fseek(tmp2, r*sizeof_base, SEEK_SET);
				fread(&d1, sizeof_base, 1, tmp2);
				fseek(cur, r*sizeof_base, SEEK_SET);
				fwrite(&d1, sizeof_base, 1, cur);
			}

			if (i1<=0) //���� ��������� ����� num1 ��� ������������ - ����� �� �����
				break;

		}
		
		fclose(tmp); remove("dopfile2");
		fclose(tmp2); remove("dopfile3");

	}
	fclose(num1);
	fclose(num2);
	fclose(cur);
	return 0;
}

int BI_deg_b(char *filenum, char *filedeg, char *filemod, char *fileres)
{												//���������! ������ �������� num,deg
	FILE *num, *deg, *mod, *res;
	if ((num = fopen(filenum, "rb+"))==NULL)
	{
		perror("Can not open file #1");
		return 1;
	}
	if ((deg = fopen(filedeg, "rb+"))==NULL)
	{
		perror("Can not open file #2");
		return 2;
	}
	if ((mod = fopen(filemod, "rb"))==NULL)
	{
		perror("Can not open file #3");
		return 3;
	}
	if ((res = fopen(fileres, "wb+"))==NULL)
	{
		perror("Can not open file #4");
		return 4;
	}
	FILE *zero, *one, *two, *tmp;
	zero=fopen("dopzero", "wb");
	one=fopen("dopone", "wb");
	two=fopen("doptwo", "wb");
	tmp=fopen("dopfile", "wb");

	unsigned char d1=0;
	int i=0, len;
	fseek(zero, i*sizeof_base, SEEK_SET);
	fwrite(&d1, sizeof_base, 1, zero);
	fclose(zero);
	fseek(tmp, i*sizeof_base, SEEK_SET);
	fwrite(&d1, sizeof_base, 1, tmp);
	//fclose(tmp);
	d1=1;
	fseek(one, i*sizeof_base, SEEK_SET);
	fwrite(&d1, sizeof_base, 1, one);
	fclose(one);
	fseek(res, i*sizeof_base, SEEK_SET);
	fwrite(&d1, sizeof_base, 1, res);
	fclose(res);
	d1=2;
	fseek(two, i*sizeof_base, SEEK_SET);
	fwrite(&d1, sizeof_base, 1, two);
	fclose(two);

	zero=fopen("dopzero", "rb");
	one=fopen("dopone", "rb");
	//two=fopen("doptwo", "rb");


	while (BI_comp_b(deg, zero)>0) //while(deg!=0)
	{

//printf("num: ");
//fseek(num, 0, SEEK_END);
//int j=ftell(num);
//for (i=0; i<j; i++)
//{
//	fseek(num, i*sizeof_base, SEEK_SET);
//	fread(&d1, sizeof_base, 1, num);
//	printf("%x ", d1);
//}
//printf("\n");
//printf("deg: ");
//fseek(deg, 0, SEEK_END);
//j=ftell(deg);
//for (i=0; i<j; i++)
//{
//	fseek(deg, i*sizeof_base, SEEK_SET);
//	fread(&d1, sizeof_base, 1, deg);
//	printf("%x ", d1);
//}
//printf("\n");
//printf("mod: ");
//fseek(mod, 0, SEEK_END);
//j=ftell(mod);
//for (i=0; i<j; i++)
//{
//	fseek(mod, i*sizeof_base, SEEK_SET);
//	fread(&d1, sizeof_base, 1, mod);
//	printf("%x ", d1);
//}
//printf("\n");
//printf("rez: ");
//fseek(res, 0, SEEK_END);
//j=ftell(res);
//for (i=0; i<j; i++)
//{
//	fseek(res, i*sizeof_base, SEEK_SET);
//	fread(&d1, sizeof_base, 1, res);
//	printf("%x ", d1);
//}
//printf("\n");
//printf("tmp: ");
//fseek(tmp, 0, SEEK_END);
//j=ftell(tmp);
//for (i=0; i<j; i++)
//{
//	fseek(tmp, i*sizeof_base, SEEK_SET);
//	fread(&d1, sizeof_base, 1, tmp);
//	printf("%x ", d1);
//}
//printf("\n");
		fclose(tmp);
		BI_mod_b(filedeg,"doptwo","dopfile");
		tmp=fopen("dopfile", "rb+");
		if (BI_comp_b(tmp, one)==0) //if(deg%2==1)
		{								//res = (res*num)mod;
			fseek(tmp, 0, SEEK_END);
			len=ftell(tmp);
			for (i=0; i<len; i++)
			{
				d1=0;
				fseek(tmp, i*sizeof_base, SEEK_SET);
				fwrite(&d1, sizeof_base, 1, tmp);
			}
			fclose(tmp);
			BI_mul_b(fileres, filenum, "dopfile");
			BI_mod_b("dopfile", filemod, fileres);
			tmp=fopen("dopfile", "rb+");
		}
									//num = (num*num)mod;
		fseek(tmp, 0, SEEK_END);
		len=ftell(tmp);
		for (i=0; i<len; i++)
		{
			d1=0;
			fseek(tmp, i*sizeof_base, SEEK_SET);
			fwrite(&d1, sizeof_base, 1, tmp);
		}
		fclose(tmp);
		BI_mul_b(filenum, filenum, "dopfile");
		BI_mod_b("dopfile", filemod, filenum);
		tmp=fopen("dopfile", "rb+");
									//deg=deg/2;
		fseek(tmp, 0, SEEK_END);
		len=ftell(tmp);
		for (i=0; i<len; i++)
		{
			d1=0;
			fseek(tmp, i*sizeof_base, SEEK_SET);
			fwrite(&d1, sizeof_base, 1, tmp);
		}

		fclose(tmp);
		BI_div_b(filedeg, "doptwo", "dopfile");
		tmp=fopen("dopfile", "rb+");
		fseek(tmp, 0, SEEK_END);
		len=ftell(tmp);
		for (i=0; i<len; i++)
		{
			fseek(tmp, i*sizeof_base, SEEK_SET);
			fread(&d1, sizeof_base, 1, tmp);
			fseek(deg, i*sizeof_base, SEEK_SET);
			fwrite(&d1, sizeof_base, 1, deg);
		}
	}
	
	fclose(zero); remove("dopzero");
	fclose(one); remove("dopone");
	remove("doptwo");
	fclose(tmp); remove("dopfile");
	fclose(num);
	fclose(deg);
	fclose(mod);
	fclose(res);
	return 0;
}

