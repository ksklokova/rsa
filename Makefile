MAIN:	libLNDll.so libLNcppDll.so 
	# компили тестовую программу для первой библиотеки
	gcc -c LN.c 
	gcc -o LN LN.o -L. -lLNDll -Wl,-rpath,. 
	# компили тестовую программу для второй библиотеки
	g++ -c LN3.cpp
	g++ -o LNcpp LN3.o -L. -lLNDll -lLNcppDll -Wl,-rpath,. 
	# компилим исполняемый файл для алгоритма RSA
	g++ -c -w RSA.cpp
	g++ -o RSA RSA.o -L. -lLNcppDll -Wl,-rpath,. 
	# компилим исполняемый файл для алгоритма Elgamal
	g++ -c -w Elgamal.cpp
	g++ -o Elgamal Elgamal.o -L. -lLNcppDll -Wl,-rpath,. 
	# компилим исполняемый файл для алгоритма Schnorr
	g++ -c -w Schnorr.cpp
	g++ -o Schnorr Schnorr.o -L. -lLNcppDll -Wl,-rpath,. 

LNDll.o:
	# компилим позиционно независимый объектный файл для первой библиотеки
	gcc -c -fPIC LNDll.c -o LNDll.o

libLNDll.so: LNDll.o
	# компилим первую библиотеку из объектного файла, указывая -shared
	gcc -shared -o libLNDll.so LNDll.o 

LNcppDll.o: libLNDll.so
	# компилим позиционно независимый объектный файл для второй библиотеки
	g++ -c -fPIC LNcppDll.cpp -o LNcppDll.o

libLNcppDll.so: LNcppDll.o 
	# компилим вторую библиотеку из объектного файла, указывая -shared
	g++ -shared -o libLNcppDll.so LNcppDll.o -L. -lLNDll -Wl,-rpath,.

clean:
	rm *.o *.so LN LNcpp RSA Elgamal Schnorr
