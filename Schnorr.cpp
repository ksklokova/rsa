#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "LNcppDll.h"

//#pragma comment (lib, "../Debug/LNcppDll.lib")

LongNum gcd (LongNum a, LongNum b, LongNum & x, LongNum & y) 
{
	LongNum zero, one;
	zero=a-a;
	if (a == zero) {
		x = zero; y = b/b;
		return b;
	}
	LongNum x1, y1;
	LongNum d = gcd (b%a, a, x1, y1);
	//x=y1-(b/a)*x1;
	one=b/a;
	zero=one*x1;
	x=y1-zero;
	y = x1;
	return d;
}

LongNum inverse(LongNum a, LongNum mod)
{
	LongNum x, y, one, zero;
	one=a/a;
	zero=a-a;
	LongNum g;
	g = gcd (a, mod, x, y);
	//g.output("gcd.txt");
	//x.output("x.txt");
	//y.output("y.txt");
	if (!(g==one)) return zero;
	else {
		x = (x % mod + mod) % mod;
		return x;
	}
}

//Schnorr.exe [-e|-d]
int main(int argc, char *argv[])
{
	if (argc!=2)	return -1;
	if (argv[1][0]!='-') return -2;
	if (argv[1][1]=='e')
	{
		//input: p.txt q.txt g.txt w.txt r.txt e.txt
		//output: y.txt x.txt s.txt 

		LongNum p, q, g, w, r, e, y, x, s, one, zero;
		p.input("p.txt", 0);
		q.input("q.txt", 0);
		g.input("g.txt", 0);
		w.input("w.txt", 0);
		r.input("r.txt", 0);
		e.input("e.txt", 0);

		one=p/p;
		zero=p-p;
		if (!((p-one)%q==zero)) return -4;
		if (!(g.deg_by_mod(q,p)==one&&!(g.deg_by_mod((p-one)/q,p)==one))) return -5;

		x=inverse(g,p);
		y=x.deg_by_mod(w,p);
		y.output("y.txt");
		x=g.deg_by_mod(r,p);
		x.output("x.txt");
		//s=(r+w*e)%q;
		y=w*e;
		x=r+y;
		s=x%q;
		s.output("s.txt");

		return 0;
	}
	else if (argv[1][1]=='d')
	{
		//input: p.txt g.txt y.txt e.txt x.txt s.txt
		//output: x1.txt

		LongNum p, g, y, e, x, s, x1, t0, t1, t2;
		p.input("p.txt", 0);
		g.input("g.txt", 0);
		y.input("y.txt", 0);
		e.input("e.txt", 0);
		x.input("x.txt", 0);
		s.input("s.txt", 0);
		
		//x1=(g.deg_by_mod(s,p)*y.deg_by_mod(e,p))%p;
		t0=g.deg_by_mod(s,p);
		t1=y.deg_by_mod(e,p);
		t2=t0*t1;
		x1=t2%p;
		x1.output("x1.txt");

		return 0;
	}
	else return -3;
}
