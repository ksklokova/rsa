//LNDll.h	������������ ��� ��� ���������� ������ � ������� �������

#include <stdio.h>

#define osn 10000		//����. �������� "�����" � ���������� ������ �������� �����
#define osn_size 4		//�������� ��������� � ���������� ������
#define max_size 1024	//����. ����� �������� ����� � ���������� ������
#define base 256		//��������� ��� �������� ������ 
#define sizeof_base sizeof(unsigned char)
#define true 1
#define false 0

typedef int mybool;

typedef struct _BigInt	//��������� "������� �����"
{
	int *digit;			//"�����" �����
	int amount;			//���������� "����"
	mybool minus;			//����
} BigInt;

//                         ������� ������ � ����������� ������� 
BigInt BI_input(FILE *f);
void BI_output(BigInt bnum, FILE *f);
int BI_comp(BigInt num1, BigInt num2);
BigInt BI_add(BigInt num1, BigInt num2);
BigInt BI_sub(BigInt num1, BigInt num2);
BigInt BI_mul(BigInt num1, BigInt num2);
BigInt BI_int_mul(BigInt num1, int n);
BigInt BI_div(BigInt num1, BigInt num2);
BigInt BI_mod(BigInt num1, BigInt num2);
BigInt BI_deg(BigInt num, BigInt deg, BigInt mod);
//                         ������� ������ � ���������� ������� 
int BI_comp_b(FILE *num1, FILE *num2);
int BI_add_b(char *filenum1, char *filenum2, char *fileres);
int BI_sub_b(char *filenum1, char *filenum2, char *fileres);
int BI_mul_b(char *filenum1, char *filenum2, char *fileres);
int BI_int_mul_b(FILE *num1, unsigned char n, FILE *res);
int BI_div_b(char *filenum1, char *filenum2, char *fileres);
int BI_mod_b(char *filenum1, char *filenum2, char *fileres);
int BI_deg_b(char *filenum, char *filedeg, char *filemod, char *fileres);
