#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "LNDll.h"

//#pragma comment (lib, "LNDll.lib")

int main(int argc, char *argv[])
{
	//int i;
	//for (i=0; i<argc; i++)
	//	printf("argv[%d]: %s\n", i, argv[i]);
	//getch();

	if (argc<5&&(argc>=7))
	{
		perror("Wrong arguments!");
		return -1;
	}

	char operation;
	if (strlen(argv[2])>1)
	{
		perror("Wrong operator!");
		return -2;
	}
	else operation=argv[2][0];

	//if ((argv[5]=="-b\0")||(argv[5]=="-b\n")||((argv[5][0]=='-')&&(argv[5][1]=='b')))
	if (argv[5] && ((argv[5][0]=='-')&&(argv[5][1]=='b')))
	{
		char *fileres;
		int l, i;
		if (argc==7&&operation!='^')
			fileres="tmpfile";
		else 	
		{
			//fileres=argv[4];
			l=strlen(argv[4]);
			fileres=(char*)malloc((l+1)*sizeof(char));
			for (i=0; i<l; i++)
				fileres[i]=argv[4][i];
			fileres[l]='\0';
		}
	
		if (operation=='+')
				BI_add_b(argv[1], argv[3], fileres);
		else if (operation=='-')
				BI_sub_b(argv[1], argv[3], fileres);
		else if (operation=='*')
				BI_mul_b(argv[1], argv[3], fileres);
		else if (operation=='/')
				BI_div_b(argv[1], argv[3], fileres);
		else if (operation=='%')
				BI_mod_b(argv[1], argv[3], fileres);
		else if (operation=='^'&&argc==7)
				BI_deg_b(argv[1], argv[3], argv[6], argv[4]);
		else 
		{
			perror("Wrong operator!\n");
			return 12;
		}

		if (argc==7&&operation!='^')
		{
			BI_mod_b(fileres, argv[6], argv[4]);
			remove("tmpfile");
		}
	}
	else
	{
		FILE *FileNum1, *FileOut, *FileNum2;
		if ((FileNum1 = fopen(argv[1], "rt"))==NULL)
		{
			perror("Can not open file #1");
			return 1;
		}
		if ((FileNum2 = fopen(argv[3], "rt"))==NULL)
		{
			perror("Can not open file #2");
			return 3;
		}
		if ((FileOut = fopen(argv[4], "wt"))==NULL)
		{
			perror("Can not open file #3");
			return 4;
		}

		BigInt bnum1, bnum2, bnum0;
		bnum1=BI_input(FileNum1);
		bnum2=BI_input(FileNum2);

		if (operation=='+')
				bnum0=BI_add(bnum1, bnum2);
		else if (operation=='-')
				bnum0=BI_sub(bnum1, bnum2);
		else if (operation=='*')
				bnum0=BI_mul(bnum1, bnum2);
		else if (operation=='/')
				bnum0=BI_div(bnum1, bnum2);
		else if (operation=='%')
				bnum0=BI_mod(bnum1, bnum2);
		else if (operation=='^'&&argc==6)
		{
			FILE *FileMod;
			BigInt bnumM;
			if ((FileMod = fopen(argv[5], "rt"))==NULL)
			{
				perror("Can not open file #4");
				return 5;
			}
			bnumM=BI_input(FileMod);

			bnum0=BI_deg(bnum1, bnum2, bnumM);
			free(bnumM.digit);
			fclose(FileMod);
		}
		else 
		{
			perror("Wrong operator!\n");
			return 2;
		}

		if (argc==6&&operation!='^')
		{
			FILE *FileMod;
			BigInt bnum0m, bnumM;
			if ((FileMod = fopen(argv[5], "rt"))==NULL)
			{
				perror("Can not open file #4");
				return 5;
			}
			bnumM=BI_input(FileMod);

			bnum0m=BI_mod(bnum0, bnumM);
			BI_output(bnum0m, FileOut);
			free(bnum0m.digit);
			free(bnumM.digit);
			fclose(FileMod);
		}
		else	BI_output(bnum0, FileOut);

		free(bnum1.digit);
		free(bnum2.digit);
		free(bnum0.digit);

		fclose(FileNum1); 
		fclose(FileNum2); 
		fclose(FileOut);
	}

	return 0;
}
