#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "LNcppDll.h"

//�����������
LongNum::LongNum()
	{
		//bin=false;
		dnum.amount=0;
		dnum.minus=false;
		bnum=NULL;
	}
LongNum::LongNum(char str[])
	{
		//bin=false;
		dnum.amount=0;
		dnum.minus=false;
		bnum=NULL;
	
		FILE *f=fopen(str, "rt");
		if (f==NULL)
			perror("Can not open file");
		dnum=BI_input(f);
		fclose(f);
	}

	//����������� �����������
LongNum::LongNum(const LongNum& a)
	{
		bin=a.bin;
		if (bin)
		{
			
			//FILE *num1, *num2;
			//if (((num1=fopen(bnum, "wb"))==NULL)||((num2=fopen(a.bnum, "rb"))==NULL))
			//	perror("Can not open File");
			//int len1, len2, i;
			//unsigned char d1;
			//fseek(num1, 0, SEEK_END);
			//len1=ftell(num1);
			//fseek(num2, 0, SEEK_END);
			//len2=ftell(num2);
			//if (len1>len2)
			//{
			//	for (i=0; i<len1; i++)
			//		if (i<len2)
			//		{
			//			fseek(num2, (len2-i)*sizeof_base, 0);
			//			fread(&d1, sizeof_base, 1, num2);
			//			fseek(num1, (len1-i)*sizeof_base, 0);
			//			fwrite(&d1, sizeof_base, 1, num1);
			//		}
			//		else
			//		{
			//			d1=0;
			//			fseek(num1, (len1-i)*sizeof_base, 0);
			//			fwrite(&d1, sizeof_base, 1, num1);
			//		}
			//}
			//else
			//{
			//	for (i=0; i<len1; i++)
			//		fseek(num2, i*sizeof_base, 0);
			//		fread(&d1, sizeof_base, 1, num2);
			//		fseek(num1, i*sizeof_base, 0);
			//		fwrite(&d1, sizeof_base, 1, num1);
			//}
			//fclose(num1);
			//fclose(num2);

			int l=strlen(a.bnum);
			bnum=new char[l];
			strcpy(bnum, a.bnum);
			//for (int i=0; i<l; i++)
			//	bnum[i]=a.bnum[i];
		}
		else 
		{
			dnum.minus=a.dnum.minus;
			dnum.amount=a.dnum.amount;
			dnum.digit=(int*)malloc(dnum.amount*sizeof(int));
			for (int i=0; i<dnum.amount; i++)
				dnum.digit[i]=a.dnum.digit[i];
		}
	}
	//������������
LongNum& LongNum::operator=(const LongNum& a)
	{
		if (this==&a)
			return *this;
		bin=a.bin;
		if (bin)
		{
			rename_b(a.bnum);
			//strcpy(bnum, a.bnum);
			//FILE *num1, *num2;
			//if (((num1=fopen(bnum, "wb"))==NULL)||((num2=fopen(a.bnum, "rb"))==NULL))
			//	perror("Can not open File");
			//int len1, len2, i;
			//unsigned char d1;
			//fseek(num1, 0, SEEK_END);
			//len1=ftell(num1);
			//fseek(num2, 0, SEEK_END);
			//len2=ftell(num2);
			//if (len1>len2)
			//{
			//	for (i=0; i<len1; i++)
			//		if (i<len2)
			//		{
			//			fseek(num2, (len2-i)*sizeof_base, 0);
			//			fread(&d1, sizeof_base, 1, num2);
			//			fseek(num1, (len1-i)*sizeof_base, 0);
			//			fwrite(&d1, sizeof_base, 1, num1);
			//		}
			//		else
			//		{
			//			d1=0;
			//			fseek(num1, (len1-i)*sizeof_base, 0);
			//			fwrite(&d1, sizeof_base, 1, num1);
			//		}
			//}
			//else
			//{
			//	for (i=0; i<len1; i++)
			//		fseek(num2, i*sizeof_base, 0);
			//		fread(&d1, sizeof_base, 1, num2);
			//		fseek(num1, i*sizeof_base, 0);
			//		fwrite(&d1, sizeof_base, 1, num1);
			//}
			//fclose(num1);
			//fclose(num2);
		}
		else 
		{
			dnum.minus=a.dnum.minus;
			dnum.amount=a.dnum.amount;
			dnum.digit=(int*)malloc(dnum.amount*sizeof(int));
			for (int i=0; i<dnum.amount; i++)
				dnum.digit[i]=a.dnum.digit[i];
		}
		return *this;
	}
	//����������
LongNum::~LongNum()
	{
		if (!bin&&dnum.amount>0)
			free(dnum.digit);
		//else delete [] bnum; 
	}
	//������������� �������� ����
void LongNum::rename_b(char *newname)
{
	if (bin)
		rename(bnum, newname);
}
	//���� �� �����
void LongNum::input(char str[], bool is_bin)
	{
		bin=is_bin;
		if (bin)
		{
			int l=strlen(str);
			bnum=new char[l];
			strcpy(bnum, str);
			//for (int i=0; i<l; i++)
			//	bnum[i]=str[i];
			//bnum[l]='\0';
			//FILE *f=fopen(bnum, "wb");
			//fclose(f);
		}
		else 
		{
			FILE *f=fopen(str, "rt");
			if (f==NULL)
				perror("Can not open file");
			dnum=BI_input(f);
			fclose(f);
		}
	}

	//����� � ���������� ����
void LongNum::output(char *str)
	{
		if (!bin)
		{
			FILE *f;
			if ((f=fopen(str, "wt"))==NULL)
				perror("Can not open file");
			BI_output(dnum, f);
			fclose(f);
		}
		else printf("ERROR: output bin file");
	}


	//��������
LongNum LongNum::operator+(LongNum& num2) const
{
	LongNum tmp;
	tmp.bin=bin;
	if (!bin&&!num2.bin)
		tmp.dnum=BI_add(dnum, num2.dnum);
	else if (bin&&num2.bin)
	{
		//tmp.bnum="tmpfile";
		//BI_add_b(bnum, num2.bnum, tmp.bnum);
	}
	return tmp;
}
	//���������
LongNum LongNum::operator-(LongNum& num2) const
{
	LongNum tmp;
	tmp.bin=bin;
	if (!bin&&!num2.bin)
		tmp.dnum=BI_sub(dnum, num2.dnum);
	else if (bin&&num2.bin)
		BI_sub_b(bnum, num2.bnum, tmp.bnum);
	return tmp;
}
	//���������
LongNum LongNum::operator*(LongNum& num2) const
{
	LongNum tmp;
	tmp.bin=bin;
	if (!bin&&!num2.bin)
		tmp.dnum=BI_mul(dnum, num2.dnum);
	else if (bin&&num2.bin)
		BI_mul_b(bnum, num2.bnum, tmp.bnum);
	return tmp;
}
	//�������
LongNum LongNum::operator/(LongNum& num2) const
{
	LongNum tmp;
	tmp.bin=bin;
	if (!bin&&!num2.bin)
		tmp.dnum=BI_div(dnum, num2.dnum);
	else if (bin&&num2.bin)
		BI_div_b(bnum, num2.bnum, tmp.bnum);
	return tmp;
}
	//������� �� �������
LongNum LongNum::operator%(LongNum& num2) const
{
	LongNum tmp;
	tmp.bin=bin;
	if (!bin&&!num2.bin)
		tmp.dnum=BI_mod(dnum, num2.dnum);
	else if (bin&&num2.bin)
		BI_mod_b(bnum, num2.bnum, tmp.bnum);
	return tmp;
}
	//��������� (��� ��������)
bool LongNum::operator==(LongNum& num2) const
{
	if (BI_comp(dnum, num2.dnum) == 0)
		return true;
	else
		return false;
}
LongNum LongNum::deg_by_mod(LongNum deg, LongNum mod)
{
	LongNum tmp;
	tmp.bin=bin;
	if (!bin&&!deg.bin&&!mod.bin)
		tmp.dnum=BI_deg(dnum, deg.dnum, mod.dnum);
	else if (bin&&deg.bin&&mod.bin)
		BI_deg_b(bnum, deg.bnum, mod.bnum, tmp.bnum);
	return tmp;
}

