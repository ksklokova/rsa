#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "LNcppDll.h"

//#pragma comment (lib, "LNcppDll.lib")

LongNum gcd (LongNum a, LongNum b, LongNum & x, LongNum & y) 
{
	LongNum zero, one;
	zero=a-a;
	if (a == zero) {
		x = zero; y = b/b;
		return b;
	}
	LongNum x1, y1;
	LongNum d = gcd (b%a, a, x1, y1);
	//x=y1-(b/a)*x1;
	one=b/a;
	zero=one*x1;
	x=y1-zero;
	y = x1;
	return d;
}

LongNum inverse(LongNum a, LongNum mod)
{
	LongNum x, y, one, zero;
	one=a/a;
	zero=a-a;
	LongNum g;
	g = gcd (a, mod, x, y);
	//g.output("gcd.txt");
	//x.output("x.txt");
	//y.output("y.txt");
	if (!(g==one)) return zero;
	else {
		x = (x % mod + mod) % mod;
		return x;
	}
}

//RSA.exe [-e|-d]
int main(int argc, char *argv[])
{
	if (argc!=2)	return -1;
	if (argv[1][0]!='-') return -2;
	if (argv[1][1]=='g')
	{
		//input: p.txt q.txt e.txt 
		//output: n.txt phi_n.txt d.txt 

		LongNum p, q, n, phi_n, d, e, m, c, one;
		p.input("p.txt", 0);
		q.input("q.txt", 0);
		e.input("e.txt", 0);

		n=p*q;
		n.output("n.txt");
		one=p/p;
		//phi_n=(p-one)*(q-one);
		p=p-one; q=q-one;
		phi_n=p*q;
		phi_n.output("phi_n.txt");
		d=inverse(e, phi_n);
		d.output("d.txt");

		return 0;
	}
	if (argv[1][1]=='e')
	{
		//input: n.txt e.txt M.txt
		//output: C.txt 

		LongNum n, e, m, c;
		n.input("n.txt", 0);
		e.input("e.txt", 0);
		m.input("M.txt", 0);

		c=m.deg_by_mod(e, n);
		c.output("C.txt");

		return 0;
	}
	else if (argv[1][1]=='d')
	{
		//input: n.txt d.txt C.txt
		//output: M.txt 

		LongNum n, d, m, c;
		n.input("n.txt", 0);
		d.input("d.txt", 0);
		c.input("C.txt", 0);

		m=c.deg_by_mod(d, n);
		m.output("M.txt");

		return 0;
	}
	else return -3;
}
