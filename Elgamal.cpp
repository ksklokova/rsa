#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "LNcppDll.h"

//#pragma comment (lib, "LNcppDll.lib")

LongNum gcd (LongNum a, LongNum b, LongNum & x, LongNum & y) 
{
	LongNum zero, one;
	zero=a-a;
	if (a == zero) {
		x = zero; y = b/b;
		return b;
	}
	LongNum x1, y1;
	LongNum d = gcd (b%a, a, x1, y1);
	//x=y1-(b/a)*x1;
	one=b/a;
	zero=one*x1;
	x=y1-zero;
	y = x1;
	return d;
}

LongNum inverse(LongNum a, LongNum mod)
{
	LongNum x, y, one, zero;
	one=a/a;
	zero=a-a;
	LongNum g;
	g = gcd (a, mod, x, y);
	//g.output("gcd.txt");
	//x.output("x.txt");
	//y.output("y.txt");
	if (!(g==one)) return zero;
	else {
		x = (x % mod + mod) % mod;
		return x;
	}
}

//Elgamal.exe [-g|-e|-d]
int main(int argc, char *argv[])
{
	if (argc!=2)	return -1;
	if (argv[1][0]!='-') return -2;
		if (argv[1][1]=='g')
	{
		//input: p.txt g.txt x.txt
		//output: y.txt

		LongNum p, g, x, y;
		p.input("p.txt", 0);
		g.input("g.txt", 0);
		x.input("x.txt", 0);

		y=g.deg_by_mod(x,p);
		y.output("y.txt");

		return 0;
	}
	if (argv[1][1]=='e')
	{
		//input: p.txt g.txt k.txt M.txt x.txt
		//output: a.txt C.txt y.txt

		LongNum p, g, k, a, m, c, y;
		p.input("p.txt", 0);
		g.input("g.txt", 0);
		k.input("k.txt", 0);
		m.input("M.txt", 0);
		y.input("y.txt", 0);

		a=g.deg_by_mod(k,p);
		a.output("a.txt");
		a=y.deg_by_mod(k,p);
		y=a*m;
		c=y%p;
		//c=(m*(y.deg_by_mod(k,p)))%p;
		c.output("C.txt");

		return 0;
	}
	else if (argv[1][1]=='d')
	{
		//input: p.txt a.txt C.txt x.txt
		//output: M.txt

		LongNum p, k, a, m, c, x, y;
		p.input("p.txt", 0);
		a.input("a.txt", 0);
		c.input("C.txt", 0);
		x.input("x.txt", 0);

		y=a.deg_by_mod(x, p);
		a=inverse(y,p);
		x=c*a;
		m=x%p;
		//m=(c*(inverse(y,p)))%p;
		m.output("M.txt");

		return 0;
	}
	else return -3;
}
