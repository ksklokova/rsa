
extern "C" {
#include "LNDll.h"
};

//#pragma comment (lib, "../LNDll.lib")

 class  LongNum
{
private:
	bool bin;
	BigInt dnum;
	char *bnum;

public:
	//�����������
	LongNum();
	LongNum(char str[]);
	//����������� �����������
	LongNum (const LongNum&);
	//������������
	LongNum& operator=(const LongNum&);
	//����������
	~LongNum();

	//������������� �������� ����
	void rename_b(char *newname);
	//���� �� �����
	void input(char *, bool);
	//����� � ���������� ����
	void output(char *);

	//��������
	LongNum operator+(LongNum&) const;
	//���������
	LongNum operator-(LongNum&) const;
	//���������
	LongNum operator*(LongNum&) const;
	//�������
	LongNum operator/(LongNum&) const;
	//������� �� �������
	LongNum operator%(LongNum&) const;
	//���������
	bool operator==(LongNum&) const;
	//���������� � ������� �� ������
	LongNum deg_by_mod(LongNum, LongNum);
};
